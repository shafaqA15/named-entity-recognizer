/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ner;
import cmu.arktweetnlp.Tagger;
import cmu.arktweetnlp.Tagger.TaggedToken;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import py4j.GatewayServer;
/**
 *
 * @author shafaq
 */
public class NER {
      Tagger tagger;
    public NER() throws IOException {
  tagger = new Tagger();
	tagger.loadModel("model.txt");


        
    }
    public HashMap<String,String>  getStack(String text) {
        List<TaggedToken> taggedTokens = tagger.tokenizeAndTag(text);
        
     HashMap<String,String> hm = new HashMap<>();
	for (TaggedToken token : taggedTokens) {
                if(hm.containsKey(token.tag)!=true)
			hm.put(token.tag,token.token);
                else{
                 String tem=  hm.get(token.tag);
                    tem+=" "+token.token;
                    hm.put(token.tag,tem);
                }
	}
    return hm;
}
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        GatewayServer gatewayServer = new GatewayServer(new NER());
        gatewayServer.start();
        System.out.println("Gateway Server Started");
    }
    
}
